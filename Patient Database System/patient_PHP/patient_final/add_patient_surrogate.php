<?php
/*
 * Created by PhpStorm.
 * User: Georgeena
 * Date: 4/17/2016
 * Time: 6:19 PM*/

$hostname = "localhost";
$username = "root";
$password = "georgeena";
$databaseName = "project_db";

$connect = mysqli_connect($hostname, $username, $password, $databaseName);

$query = "SELECT Patient_Id, First_Name, Last_Name FROM patient ";

$result1 = mysqli_query($connect, $query);
?>


<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="homecss.css">
</head>
<body>

<div class="jumbotron">
    <div class="container">

        <h2> Patient Database System </h2>
    </div>
</div>

<div class="container">
    <h2> Add Surrogate Details </h2>
</div>

<form action="add_surrogate_success.php" method="post" role="form">

    <div class="container">
        <div class="row">
            <select name="patientid">
                <?php
                while($row1 = mysqli_fetch_array($result1, MYSQLI_NUM)) { ?>
                    <option value="<?php echo $row1[0];?>">
                        <?php echo $row1[2]; ?>, <?php echo $row1[1];?></option>
                <?php } ?>
            </select>
        </div>
    </div>


    <div class="container">
       <div class="row">
          <div class="col-sm-3">
              <div class="form-group">
                 <label for="sur_name">
                   Surrogate Name :
                 </label>
                  <input type ="text" class="form-control" name="sur_name">
              </div>
          </div>
       </div>


    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
               <label for="relation">
                   Relationship :
               </label>
                <input type ="text" class="form-control" name="relation">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
            <label for="age">
                Age :
                </label>
                <input type="text" class="form-control" name="age">
            </div>
        </div>
    </div>

        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="phone_no">
                        Phone No :
                    </label>
                    <input type="text" class="form-control" name="phone_no">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="waive_form" name="waive">
                    <a href="http://www.massmed.org/patient-care/health-topics/health-care-proxies-and-end-of-life-care/massachusetts-health-care-proxy---information,-instructions-and-form-(pdf)/">
                        Patient Proxy Form
                    </a>
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
</div>
</form>
</body>

</html>



