<?php
/**
 * Created by PhpStorm.
 * User: Georgeena
 * Date: 4/20/2016
 * Time: 1:43 PM
 */?>

<html>
<head>
    <title>
        Update Patients
    </title>
    <link href="homecss.css" rel="stylesheet" type="text/css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<body>
<div class="jumbotron">
    <div class="container">

        <h2> Patient Database System </h2>
    </div>
</div>
<div class="container">
    <div class="row">
        <h3> SELECT PATIENT TO UPDATE </h3>
    </div>

    <div class="row">
        <h4> PATIENT LIST </h4>
        <?php
        $connection = mysqli_connect("localhost", "root", "georgeena", "project_db");
        if (!$connection) {
            die("Database selection failed: " . mysqli_error($connection));
        }
        if (isset($_GET['submit'])) {
            $fn = $_GET['fn'];
            $ln = $_GET['ln'];
            $ms = $_GET['ms'];
            $gen = $_GET['gen'];
            $addr = $_GET['addr'];
            $id=$_GET['id'];

            $query = mysqli_query($connection,
                "update patient set
                    First_Name='$fn'
                    ,Last_Name='$ln'
                    ,Marital_Status='$ms'
                    ,Gender='$gen'
                    , Address='$addr' 
                    ,Street ='$street'
                    ,City='$city'
                    ,State='$state'
                    ,Cellphone_No='$cellphone_no'
                    ,Email_Address='$email_address'
                    ,Emergency_Contact_Name='$emergency_contact_name'
                    where Patient_Id='$id'");

        }
        $query = mysqli_query($connection, "select * from patient");
        while ($row = mysqli_fetch_array($query)) {
            echo "<b><a href='updatepatient.php?update={$row['Patient_Id']}'>{$row['First_Name']}</a></b>";
            echo "<br />";
        }
        ?>

    </div>


    <?php
    if (isset($_GET['update'])) {
        $update = $_GET['update'];
        $query = mysqli_query($connection, "select * from patient where
                 Patient_Id=$update");

        while ($row1 = mysqli_fetch_array($query)) {
            echo "<form class='form' method='GET'>";
            echo "<h2>Update Form</h2>";
            echo "<hr/>";
            echo "<input class='input' type='hidden' name='id' value='{$row1['Patient_Id']}' />";
            echo "<input class='input' type='text' name='fn' value='{$row1['First_Name']}' />";
            echo "<br />";
            echo "<label>" . "Name:" . "</label>" . "<br />";
            echo "<input class='input' type='text' name='ln' value='{$row1['Last_Name']}' />";
            echo "<br />";
            echo "<label>" . "Email:" . "</label>" . "<br />";
            echo "<input class='input' type='text' name='ms' value='{$row1['Marital_Status']}' />";
            echo "<br />";
            echo "<label>" . "Mobile:" . "</label>" . "<br />";
            echo "<input class='input' type='text' name='gen' value='{$row1['Gender']}' />";
            echo "<br />";
            echo "<label>" . "Address:" . "</label>" . "<br />";
            echo "<textarea rows='15' cols='15' name='addr'>{$row1['Address']}";
            echo "</textarea>";
            echo "<br />";
            echo "<label>" . "Street:" . "</label>" . "<br />";
            echo "<input class='input' type='text' name='street' value='{$row1['Street']}' />";
            echo "<br />";
            echo "<label>" . "City:" . "</label>" . "<br />";
            echo "<input class='input' type='text' name='city' value='{$row1['City']}' />";
            echo "<br />";
            echo "<label>" . "State:" . "</label>" . "<br />";
            echo "<input class='input' type='text' name='state' value='{$row1['State']}' />";
            echo "<br />";
            echo "<label>" . "Cellphone Number:" . "</label>" . "<br />";
            echo "<input class='input' type='text' name='cellphone_no' value='{$row1['Cellphone_No']}' />";
            echo "<br />";
            echo "<label>" . "Email ID:" . "</label>" . "<br />";
            echo "<input class='input' type='text' name='email_address' value='{$row1['Email_Address']}' />";
            echo "<br />";
            echo "<label>" . "Emergency Contact Name:" . "</label>" . "<br />";
            echo "<input class='input' type='text' name='emergency_contact_name' 
            value='{$row1['Emergency_Contact_Name']}' />";
            echo "<br />";
            echo "<input class='submit' type='submit' name='submit' value='update' />";
            echo "</form>";
        }
    }
    if (isset($_GET['submit'])) {
        echo '<div class="form" id="form3"><br><br><br><br><br><br>
                <Span> Patient Data Updated </span></div>';
    }
    ?>

    <div class="clear"></div>
</div>
<div class="clear"></div>
</div>
</div> <?php mysqli_close($connection); ?>
</body>
</html>


</div>